
const {
    leerAlumnos,
    guardarAlumno
} = require('../utils/functions')

const todos = async (request,response) =>{
    var alumnos = await leerAlumnos()
    response.render('alumnos/todos',{locals:{data:alumnos}})
}
const nuevo = async (request,response) =>{
    const data = await request.body
    const respuesta = guardarAlumno(data)
    return response.json(respuesta)
}
const editar = async (request,response) =>{
    const {id,nombre,direccion,curso}=await request.body
    var alumnos = await leerAlumnos()
    alumnos.forEach(alumno => {
        if(alumno.id==id){
            console.log(alumno)
            return response.json("encontrado")
        }
    });
    return response.json("no encontrado")
    }

const eliminar = () =>{

}
module.exports = {
    nuevo,
    todos,
    editar, 
    eliminar
}