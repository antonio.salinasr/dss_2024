var functions = require("../utils/functions")

const hello =  (request, response )=> {
    response.send( 'Hello World!!!')
}
const v3recibedatos = async (request,response) =>{
    var data = {nombre, email, mensaje} = await request.body
    console.log(data)
    comprueba = await functions.compruebaInyeccionJs(data)
    if(comprueba){
        response.render('ejercicio1104/error',{locals: data})
    }else{
        response.render('ejercicio1104/mostrar',{locals: data})
    }  
}
const goodbye = ( request, response )=> {
    response.send( 'bye World!!!')
}
const obtenerResultados = ( request, response )=> {
    const arr = []
    for(i = 1;i<10;++i){
        arr.push(i)
    }
    response.send(arr);
}
const sayMyName = (request,response)=>{
    const myName = request.query
    console.log(myName)
    response.send(`Mi nombre es ${myName.name} ${myName.apellido}`)
}
const direccion = (request,response)=>{
    const direccion = request.query
    console.log(direccion)
    response.send(`Mi direccion es: 
    Calle: ${direccion.calle} 
    Numero: ${direccion.numero},
    Ciudad: ${direccion.ciudad},
    Region: ${direccion.region},`)
}
const pruebaPost = (request,response)=>{
    console.log(request.body)
    response.send(`Nombre ${request.body.nombre}`)
}
const sumarGet = (request,response)=>{
    const {numero1,numero2,numero3,numero4} = request.query
    console.log(numero4)
    const suma = parseInt(numero1) 
    + parseInt(numero2) 
    + parseInt(numero3)
    response.send(`La suma es ${suma}`)
}
const sumarPost = (request,response)=>{
    const {numero1,numero2,numero3} = request.body
    console.log(request.body)
    // console.log(numero1)
    // console.log(numero2)
    // console.log(numero3)
    const suma = parseInt(numero1) 
    + parseInt(numero2) 
    + parseInt(numero3)
    response.send(`La suma es ${suma}`)
}
const sumarArr = (request,response)=>{
    const {arrNum} = request.body
    var suma = 0
    arrNum.forEach(numero => {
        suma+=numero
    });
    response.send(`La suma es ${suma}`)
}
const holamundo = (request,response) =>{
    response.render('holamundo')
}
const login = (request,response) =>{
    response.render('login')
}
const recibedatosv1 = async (request,response) =>{
    var {usuario,contrasena,tipo} = await request.body
    if(tipo == "reemplazo"){
       data =  await functions.limpiarCodigoReemplazo({usuario,contrasena})
       console.log(data)
       response.render('recibedatos',{locals: data})
    }else{
        data =  await functions.limpiarCodigoSanitizer({usuario,contrasena})
       console.log(data)
       response.render('recibedatos',{locals: data})
    }
    
}
const recibedatosv2 = async (request,response) =>{
    var {usuario,contrasena,tipo} = await request.body
    data =  await functions.limpiarCodigoSanitizer({usuario,contrasena})
    comprueba = await functions.compruebaInyeccionJs(request.body)
    if(comprueba){
        response.render('error',{locals: data})
    }else{
        response.render('recibedatos',{locals: data})
    }  
}
const ingresar = (request,response)=>{
    response.render('ejercicio1104/ingresar')
}
module.exports ={
    hello,
    v3recibedatos,
    goodbye,
    obtenerResultados,
    sayMyName,
    direccion,
    pruebaPost,
    sumarGet,
    sumarPost,
    sumarArr,
    holamundo,
    login,
    recibedatosv1,
    recibedatosv2,
    ingresar
}