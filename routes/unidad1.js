var express = require('express');
var router = express.Router();
var un1 = require("../controllers/unidad1Controller")

router.get('/hello',un1.hello);
router.get('/goodbye', un1.goodbye);
router.get('/v1/api/obtenerResultados', un1.obtenerResultados);
router.get('/sayMyName',un1.sayMyName)
router.get('/direccion',un1.direccion)
router.post('/pruebaPost',un1.pruebaPost)
router.get('/sumar',un1.sumarGet)
router.post('/sumar',un1.sumarPost)
router.post('/sumarArr',un1.sumarArr)
router.get('/holamundo',un1.holamundo)
router.get('/login',un1.login)
router.post('/recibedatos',un1.recibedatosv1)
router.post('/v2/recibedatos',un1.recibedatosv2)
router.post('/v3/recibedatos',un1.v3recibedatos)
router.get('/ingresar',un1.ingresar)

module.exports = router;