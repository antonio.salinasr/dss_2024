var express = require('express');
var router = express.Router();
var al = require("../controllers/alumnoController")

router.get('/todos',al.todos)
router.post('/nuevo',al.nuevo)
router.post('/editar',al.editar)
router.post('/eliminar',al.eliminar)

module.exports = router;