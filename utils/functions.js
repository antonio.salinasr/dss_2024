const { v4: uuidv4 } = require('uuid');
const { GoogleGenerativeAI } = require("@google/generative-ai");
require('dotenv').config()
const genAI = new GoogleGenerativeAI(process.env.API_KEY);
const model = genAI.getGenerativeModel({ model: "gemini-pro"});
var sanitizer = require('sanitizer');
var fs = require('fs');
const limpiarCodigoSanitizer = async(datos) =>{
    var {usuario,contrasena} = datos 
    usuario = sanitizer.escape(usuario)
    contrasena = sanitizer.escape(contrasena)
    //  usuario= await sanitizer.sanitize(usuario)
    //  contrasena= await sanitizer.sanitize(contrasena)
    // usuario = await sanitizer.normalizeRCData(usuario)
    // contrasena = await sanitizer.normalizeRCData(contrasena)
    // usuario = sanitizer.unescapeEntities(usuario)//->no
    // contrasena = sanitizer.unescapeEntities(contrasena)//->no
    const normalized = {usuario,contrasena}
    return normalized
}
const compruebaInyeccionJs = async (request) =>{
    const prompt = `¿Existe codigo javascript 
                    en el siguiente objeto : 
                    ${JSON.stringify(request)}?.
                    responde solo true o false`
    const result = await model.generateContent(prompt);
    const response = await result.response;
    const text = response.text();
    var respuesta = false
    text == "true" ? respuesta=true:respuesta=false
    console.log(respuesta)
    return(respuesta)
      
}
const limpiarCodigoReemplazo = async(datos) =>{
    const prohibidos = ['<','>',"script","alert","timeOut","(",")"]
    var {usuario,contrasena} = datos 
    prohibidos.forEach(car =>{
        contrasena = contrasena.replaceAll(car,'')
        usuario = usuario.replaceAll(car,'')
    })
    datos = {usuario,contrasena}
    console.log(datos)
    return datos
}
const leerAlumnos = () =>{
    try {
        const alumnos = JSON.parse(fs.readFileSync("./data/data.txt", 'utf8'));
  
        if (alumnos==undefined){
            return []
        }else{
            return alumnos
        }
    } catch (err) {
        console.error(err);
        return [];
    }
    
}
const guardarAlumno = (alumno) =>{
    try{
        var todos = leerAlumnos() 
        alumno.id = uuidv4()
        console.log("console todos",todos);
        todos.push(alumno)
        fs.writeFileSync("./data/data.txt",JSON.stringify(todos)+"\n" )
        return true
    } catch (err) {
        console.error(err);
        return false;
    }
    
}
const editarAlumno = (alumno) =>{
<<<<<<< HEAD
 console.log("voy a edetar un alumno")
=======
    console.log("Wena naty")
>>>>>>> cambio2
}
module.exports = {
    limpiarCodigoSanitizer,
    compruebaInyeccionJs,
    limpiarCodigoReemplazo,
    leerAlumnos,
    guardarAlumno
}